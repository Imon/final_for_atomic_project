<?php
use src\Bitm\SEIP106607\Book;
include_once ('../../../'.'vendor/autoload.php');

$book = new Book();
$books = $book->index();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Book list</title>
    </head>
    <body>
        <table border ='1'>
            <thead>
                <tr>
                    <td>SL. NO.</td>
                    <td>Title</td>
                    <td>Author</td>
                    <td>Image</td>
                    <td colspan="3" style="text-align: center;">Action</td>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    $i=0;
                    foreach($books as $list){
                              $i++;
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><a href="view.php"><?php echo $list['title'];?></a></td>
                    <td><?php echo $list['author'];?></td>
                    <td><?php echo $list['coverpage']; ?></td>
                    <td>View</td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="delete"/>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            
        </table>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a></br> 
        </div>
    </body>
</html>