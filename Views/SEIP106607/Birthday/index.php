<?php
use src\Bitm\SEIP106607\Birthday;
include_once ('../../../'.'vendor/autoload.php');

$Info = new Birthday();
$Infos = $Info->Index();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Creating Birthday details</title>
    </head>
    <body>
        <table border="1">
            <thead>
                <td>SL. No.</td>
                <td>Name</td>
                <td>Birthday</td>
                <td colspan="4" style="text-align: center">Action</td>
            </thead>
            <tbody>
                
                <?php
                $i=0;
                foreach ($Infos as $details){
                    $i++;
                
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $details['name'];?></td>
                    <td><?php echo $details['birthday'];?></td>
                    <td>View</td>
                    <td>
                        <form action="Update.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $details['id'];?>"/>
                                    <input type="submit" value="Edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="trash.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $details['id'];?>"/>
                                    <input type="submit" value="Trash"/>
                            </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $details['id'];?>"/>
                                    <input type="submit" value="Delete"/>
                            </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        </br></br>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a> 
        <a href="recycle.php"><input type="submit" name="create" value="Recycle Bin"></a>
        </div>
        
    </body>
</html>

