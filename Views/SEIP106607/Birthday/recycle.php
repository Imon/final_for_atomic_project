<?php
use src\Bitm\SEIP106607\Birthday;
include_once ('../../../'.'vendor/autoload.php');


//var_dump($_POST);die();
$Delete =new Birthday();
$trashed = $Delete->recycle();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Recycle Bin</title>
        
    </head>
    <body class="body" style="background-color: #9acfea">
        
        <table border='1' style="text-align: center">
            <thead class="TabHead">
                <tr>
                    <td>SL.No.</td>
                    <td>Book Title</td>
                    <td>Author Name</td>
                    <td colspan="2">Action</td>
                    
                </tr>
            </thead>
            <tbody class="tabBody">
                <?php 
                $i = 0;
                foreach ($trashed as $recycle){
                    $i++;
                    ?>
                <tr>
                    
                    <td><?php echo $i;?></td>
                    <td><?php echo $recycle['name'];?></td>
                    <td><?php echo $recycle['birthday'];?></td>
                    
                    <td>
                        <form action="recover.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $recycle['id'];?>"/>
                            <input type="submit" value="Recover"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $recycle['id'];?>"/>
                            <input type="submit" value="Parmanently Delete"/>
                        </form>
                    </td>
                    
                </tr>
                <?php }?>
            </tbody>
        </table>
        <div>
            <a href="index.php"><input type="submit" name="create" value="Back To List"></a> 
        </div>
    </body>
</html>