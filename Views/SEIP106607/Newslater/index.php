<?php
use src\Bitm\SEIP106607\Newslater;
include_once ('../../../'.'vendor/autoload.php');

$Newslater = new Newslater();
$Newslaters = $Newslater->index();

?>
<!DOCTYPE html>
<html>
    <head>
        <title>Newslater</title>
    </head>
    <body>
        <table border ='1'>
            <thead>
                <tr>
                    <td>SL. NO.</td>
                    <td>Name</td>
                    <td>Newslater</td>
                    <td colspan="3" style="text-align: center;">Action</td>
                </tr>
            </thead>
            <tbody>
                
                <?php 
                    $i=0;
                    foreach($Newslaters as $list){
                              $i++;
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><a href="view.php"><?php echo $list['name'];?></a></td>
                    <td><?php echo $list['newslater'];?></td>
                    <td>View</td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $list['id'];?>"/>
                            <input type="submit" value="Delete"/>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            
        </table>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a></br> 
        </div>
    </body>
</html>