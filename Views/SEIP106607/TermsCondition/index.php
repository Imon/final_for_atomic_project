<?php
use src\Bitm\SEIP106607\TermsCondition;
include_once ('../../../'.'vendor/autoload.php');

$condition = new TermsCondition();
$rule = $condition->index();
echo "<hr \>";
?>

<!DOCTYPE html>
<html>
    <head>
        <title>All data </title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <td>SL.NO.</td>
                    <td>Name</td>
                    <td>Condition</td>
                    <td colspan="3">Action</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 0;
                foreach ($rule as $terms){
                    $i++;
                
                ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $terms['name']; ?></td>
                    <td><?php echo $terms['condition']; ?></td>
                    <td>
                        <form action="#" method="post">
                            <input type="hidden" name="id" value="<?php echo $terms['id']; ?>"/>
                            <input type="submit" value="View"/>
                        </form>
                    </td>
                    <td>
                        <form action="edit.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $terms['id']; ?>"/>
                            <input type="submit" value="Edit"/>
                        </form>
                    </td>
                    <td>
                        <form action="delete.php" method="post">
                            <input type="hidden" name="id" value="<?php echo $terms['id']; ?>"/>
                            <input type="submit" value="Delete"/>
                        </form>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a></br> 
        </div>
    </body>
</html>