<?php
use src\Bitm\SEIP106607\mobile;
include_once ("../../../"."vendor/autoload.php");

$Saved=new mobile();
$data = $Saved->index();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
            <title>Mobile lists</title>
    </head>
    <body>
       
        <div>
            <h1>The Mobile List.</h1>
        </div>
            <table border="1">
                <thead>
                    <tr>
                        <td>Sl.No.</td>
                        <td>Title</td>
                        <td>Brand</td>
                        <td colspan="3" style="text-align: center;">Action</td>
                    </tr>
                </thead>
                <tbody>
                     
                <?php
                $i=0;
                foreach ($data as $info){
                    $i++;
                
                ?>
                   
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $info['title'];?></td>
                        <td><?php echo $info['brand'];?></td>
                        <td>View</td>
                        <td><form action="edit.php" method="post">
                                    <input type="hidden" name="id" value="<?php echo $info['id'];?>"/>
                                    <input type="submit" value="Edit"/>
                        </form></td>
                        <td>
                            <form action="delete.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $info['id'];?>"/>
                                <input type="submit" value="Delete"/>
                            </form>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        
        <div>
        <a href="create.php"><input type="submit" name="create" value="Create New List"></a> <a href="../../../index.php"><input type="submit" name="create" value="Back to home"></a></br> 
        </div>
        

</body>
</html>