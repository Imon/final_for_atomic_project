<?php
require_once 'Registration.php';

//use src\Bitm\SEIP106607\Registration\Registration;
//include_once ('../../../../'.'vendor/autoload.php');

$users = new Registration();
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Login Form</title>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <?php
       
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $name       = $_POST['name'];
            $user_name  = $_POST['user_name'];
            $email      = $_POST['email'];
            $password   = $_POST['password'];
            $repassword = $_POST['repassword'];
        
            if(empty($name or $user_name or $email)){
                echo "<span style='color: red; text-align: center;'><h1>Please any of the field should not be empty.</h1></span>";
            }
                if($password !==$repassword){
                    echo "<span style='color: red; text-align: center;'><h1>Password Not Matched!!!!</h1></span>";
                }  else {
                        $password   = md5($password);
                        $repassword = md5($repassword);
                        $success = $users->register($name, $user_name, $email, $password, $repassword); 
                        if($success){
                            echo "<span style='color:green; text-align:center;'><h2>Registration complete.<a href='Login.php'>click here</a> for LogIn.</h2></span>.'";
                            
                        }
                    }
            
        }
        
        ?>
        
        
        
        <form action="" method="post" class="form-group">
            <div class="row">
                
                <div class="col-md-12" style="padding-top: 2%;">
                    <div class="col-md-offset-3 col-md-2">
                        <label>Name</label>
                    </div>
                    <div class="col-md-2">
                            <input type="text" name="name" class="form-control" placeholder="Enter you full name"/>
                    </div>
                </div>
                
                <div class="col-md-12" style="padding-top: 2%;">
                    <div class="col-md-offset-3 col-md-2">
                        <label>User name</label>
                    </div>
                    <div class="col-md-2">
                            <input type="text" name="user_name" class="form-control" placeholder="Enter the user name"/>
                    </div>
                </div>
                    <div class="col-md-12" style="padding-top: 2%;">
                        <div class="col-md-offset-3 col-md-2">
                            <label>Email</label>
                        </div>
                        <div class="col-md-2">
                            <input type="email" name="email" class="form-control" placeholder="examle@yahoo.com"/>
                        </div> 
                    </div>
                    <div class="col-md-12" style="padding-top: 2%;">
                        <div class="col-md-offset-3 col-md-2">
                            <label>Password</label>
                        </div>
                        <div class="col-md-2">
                            <input type="password" name="password" class="form-control"/>
                        </div> 
                    </div>
                    <div class="col-md-12" style="padding-top: 2%;">
                        <div class="col-md-offset-3 col-md-2">
                            <label>Re-Enter Password</label>
                        </div>
                        <div class="col-md-2">
                            <input type="password" name="repassword" class="form-control"/>
                        </div> 
                    </div>
                    <div  class="col-md-12" style="padding-top: 2%;">
                        <div class="col-md-offset-3 col-md-4">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Sign Up"/>
                    </div>
                    </div>
                </div>
            
        </form>
        <div class="col-md-12" style="padding-top: 2%;">
            <div class="col-md-offset-3 col-md-4">
                <a href="Login.php"><input type="submit" class="btn btn-primary btn-lg btn-block" value="Already Regiester"/></a>
            </div>
        </div>
    </body>
</html>
