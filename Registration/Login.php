<?php
require_once 'Registration.php';

//use src\Bitm\SEIP106607\Registration\Registration;
//include_once ('../../../../'.'vendor/autoload.php');
$users = new Registration();
if ($users->getSession()){
    header("location:../index.php");
    exit();
}
?>
<!DOCTYPE html>

<html>
    <head>
        <title>Login Form</title>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
         <?php
       
        if($_SERVER['REQUEST_METHOD'] == "POST"){
            
            $user_name  = $_POST['user_name'];
            $password   = $_POST['password'];
            
            
            if(empty($user_name or $password)){
                echo "<span style='color: red; text-align: center;'><h1>Field should not be empty.</h1></span>";
            }  else {
                $login = $users->login($user_name,$password);
                if ($login){
                    header("location:../index.php");
                }  else {
                    echo "<span style='color: red; text-align: center;'><h1>error...User name or password does not match.</h1></span>";
                }
            }
        
            
        }
        
        ?>
        <form action="#" method="post" class="form-group">
            <div class="row">
                <div class="col-md-12" style="padding-top: 2%;">
                    <div class="col-md-offset-3 col-md-2">
                        <label>User name</label>
                    </div>
                    <div class="col-md-2">
                            <input type="text" name="user_name" class="form-control" placeholder="Enter the user name"/>
                    </div>
                </div>
                    
                    <div class="col-md-12" style="padding-top: 2%;">
                        <div class="col-md-offset-3 col-md-2">
                            <label>Password</label>
                        </div>
                        <div class="col-md-2">
                            <input type="password" name="password" class="form-control"/>
                        </div> 
                    </div>
                    
                    <div  class="col-md-12" style="padding-top: 2%;">
                        <div class="col-md-offset-3 col-md-4">
                        <input type="submit" class="btn btn-primary btn-lg btn-block" value="Login"/>
                    </div>
                    </div>
                </div>
            

            
        </form>
    </body>
</html>
