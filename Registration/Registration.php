<?php
//namespace src\Bitm\SEIP106607\Registration;
//require 'SignUp.php';
class Registration {
    public $id = "";
    public $name = "";
    public $user_name = "";
    public $email ="";
    
    public function __construct() {
        global $pdo;
        try{
            $pdo = new PDO('mysql: host=localhost; dbname=atomicproject','root','');
            
        }catch (PDOException $e){
            exit("Database Eror");
        }
      }
    public function register($name,$user_name,$email,$password,$repassword){
            global $pdo;
            $querey = $pdo->prepare("SELECT id FROM users WHERE user_name = ? AND email = ?");
            $querey->execute(array($user_name,$email));
            $num = $querey->rowCount();
            if($num == 0){
                $querey = $pdo->prepare("INSERT INTO users (name,user_name,email,password,repassword) VALUES (?,?,?,?,?)");
                $querey->execute(array($name,$user_name,$email,$password,$repassword));
                return TRUE;
            }  else {
                return print "<span style='color:red;text-align:center;'><h1>User Name or Email already exist</h1></span>";
            }
        
    }

    public function login($user_name,$password){
        global $pdo;
        $querey = $pdo->prepare("SELECT id,user_name FROM users WHERE user_name = ? AND password = ?");
        $querey->execute(array($user_name,$password));
        $userData = $querey->fetch();
        
        $num = $querey->rowCount();
        
        if($num == 1){
            session_start();
            $_SESSION['login']=TRUE;
            $_SESSION['uid']= $userData['id'];
            $_SESSION['uname']= $userData['user_name'];
            $_SESSION['login_msg']='login successfully';
            return TRUE;
            
        }  else {
            return FALSE;
        }
    }
    public function getSession(){
        return @$_SESSION['login'];
    }
}
